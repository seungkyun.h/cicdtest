export const testFunction = (text: string, afterText: string) => {
  if (text) {
    message(text);
    message(afterText);
    return text;
  } else {
    throw new Error("Must provide argument: text");
  }
};

const message = (text: string) => {
  console.log("Message: ", text);
};
